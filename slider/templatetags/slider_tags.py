# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django import template
from slider.models import Slide

register = template.Library()


@register.inclusion_tag('slider/tags/slider.html')
def slider():
    slides = Slide.objects.all()
    return {
        'slides': slides
    }
