# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import
from django.contrib import admin
from slider.models import Slide

admin.site.register(Slide)
