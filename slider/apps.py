# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import
from django.apps import AppConfig


class SliderConfig(AppConfig):
    name = 'slider'
    verbose_name = 'Слайдер'
