# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import
from django.core.urlresolvers import reverse
from django.contrib.sitemaps import Sitemap
from django.contrib.flatpages.sitemaps import FlatPageSitemap
from oscar.core.loading import get_model
from articles.sitemaps import ArticleSitemap

Product = get_model('catalogue', 'Product')
Category = get_model('catalogue', 'Category')


class StaticSitemap(Sitemap):
    def items(self):
        return ['promotions:home', ]

    def location(self, obj):
        return reverse(obj)


class ProductSitemap(Sitemap):
    def items(self):
        return Product.browsable.all()


class CategorySitemap(Sitemap):
    def items(self):
        return Category.objects.all()


SITEMAPS = {
    'static': StaticSitemap,
    'category': CategorySitemap,
    'product': ProductSitemap,
    'pages': FlatPageSitemap,
    'articles': ArticleSitemap
}
