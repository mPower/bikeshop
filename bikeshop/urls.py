from __future__ import absolute_import, unicode_literals

from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.contrib.sitemaps.views import sitemap
from oscar.app import application
from .sitemaps import SITEMAPS

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^redactor/', include('redactor.urls')),
    url(r'^articles/', include('articles.urls')),
    url(r'^robots\.txt', include('robots.urls')),
    url(r'^sitemap\.xml$', sitemap, {'sitemaps': SITEMAPS}, name='sitemap'),
    url(r'', include(application.urls)),
]

if settings.DEBUG:
    from django.conf.urls.static import static

    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
