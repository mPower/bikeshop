# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from django import template

register = template.Library()


@register.simple_tag
def field(fld, **kwargs):
    fld.field.widget.attrs.update(kwargs)
    return fld
