# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.urls import reverse
from django.utils.translation import ugettext as _

from oscar.apps.checkout.session import CheckoutSessionMixin as CoreCheckoutSessionMixin
from oscar.apps.checkout.session import exceptions
from shipping.methods import SelfPickup


class CheckoutSessionMixin(CoreCheckoutSessionMixin):
    def check_a_valid_shipping_address_is_captured(self):
        # Check that shipping address has been completed

        shipping_method = self.get_shipping_method(
            basket=self.request.basket)

        if not self.checkout_session.is_shipping_address_set() and shipping_method:
            code = getattr(shipping_method, 'code', None)
            if code and code != SelfPickup.code:
                raise exceptions.FailedPreCondition(
                    url=reverse('checkout:shipping-address'),
                    message=_("Please choose a shipping address")
                )

        # Check that the previously chosen shipping address is still valid
        shipping_address = self.get_shipping_address(
            basket=self.request.basket)

        if not shipping_address and shipping_method:
            code = getattr(shipping_method, 'code', None)
            if code and code != SelfPickup.code:
                raise exceptions.FailedPreCondition(
                    url=reverse('checkout:shipping-address'),
                    message=_("Your previously chosen shipping address is "
                              "no longer valid.  Please choose another one")
                )
