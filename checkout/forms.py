# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import
from oscar.apps.checkout.forms import ShippingAddressForm as CoreShippingAddressForm


class ShippingAddressForm(CoreShippingAddressForm):
    class Meta(CoreShippingAddressForm.Meta):
        fields = ('first_name', 'last_name', 'country', 'line4', 'line1', 'phone_number', 'notes')

    def __init__(self, *args, **kwargs):
        super(ShippingAddressForm, self).__init__(*args, **kwargs)
        self.fields['line1'].label = 'Адрес'
        self.fields['notes'].label = 'Комментарий'
        self.fields['phone_number'].label = 'Номер телефона'
        self.fields['phone_number'].required = True
