# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import
from django.apps import AppConfig


class ArticlesDashboardConfig(AppConfig):
    label = 'articles_dashboard'
    name = 'dashboard.articles'
    verbose_name = 'Статьи'