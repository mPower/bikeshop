# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import
from django.conf import settings
from django.core.urlresolvers import reverse_lazy
from django.views import generic
from django.views.generic import ListView

from articles.models import Article
from .forms import ArticleUpdateForm


class ArticleListView(ListView):
    template_name = 'dashboard/articles/index.html'
    model = Article
    paginate_by = settings.OSCAR_DASHBOARD_ITEMS_PER_PAGE


class ArticleCreateView(generic.CreateView):
    template_name = 'dashboard/articles/update.html'
    model = Article
    form_class = ArticleUpdateForm
    context_object_name = 'article'
    success_url = reverse_lazy('dashboard:article-list')

    def get_context_data(self, **kwargs):
        ctx = super(ArticleCreateView, self).get_context_data(**kwargs)
        ctx['title'] = 'Добавить статью'
        return ctx


class ArticleUpdateView(generic.UpdateView):
    template_name = 'dashboard/articles/update.html'
    model = Article
    form_class = ArticleUpdateForm
    context_object_name = 'article'
    success_url = reverse_lazy('dashboard:article-list')

    def get_context_data(self, **kwargs):
        ctx = super(ArticleUpdateView, self).get_context_data(**kwargs)
        ctx['title'] = self.object.title
        return ctx


class ArticleDeleteView(generic.DeleteView):
    template_name = 'dashboard/articles/delete.html'
    model = Article
    success_url = reverse_lazy('dashboard:article-list')
