# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import
from django.conf.urls import url
from oscar.core.application import DashboardApplication
from oscar.core.loading import get_class


class ArticlesManagementApplication(DashboardApplication):
    name = None
    default_permissions = ['is_staff', ]

    list_view = get_class('dashboard.articles.views', 'ArticleListView')
    create_view = get_class('dashboard.articles.views', 'ArticleCreateView')
    update_view = get_class('dashboard.articles.views', 'ArticleUpdateView')
    delete_view = get_class('dashboard.articles.views', 'ArticleDeleteView')

    def get_urls(self):
        """
        Get URL patterns defined for flatpage management application.
        """
        urls = [
            url(r'^$', self.list_view.as_view(), name='article-list'),
            url(r'^create/$', self.create_view.as_view(), name='article-create'),
            url(r'^update/(?P<pk>[-\w]+)/$',
                self.update_view.as_view(), name='article-update'),
            url(r'^delete/(?P<pk>\d+)/$',
                self.delete_view.as_view(), name='article-delete')
        ]
        return self.post_process_urls(urls)


application = ArticlesManagementApplication()
