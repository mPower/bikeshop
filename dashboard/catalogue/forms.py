from django import forms
from treebeard.forms import movenodeform_factory
from oscar.core.loading import get_model
from oscar.apps.dashboard.catalogue import forms as catalogue_forms

Category = get_model('catalogue', 'Category')
Product = get_model('catalogue', 'Product')

CategoryForm = movenodeform_factory(
    Category,
    fields=['name', 'description', 'image', 'product_class'])


class ProductForm(catalogue_forms.ProductForm):
    class Meta(catalogue_forms.ProductForm.Meta):
        fields = catalogue_forms.ProductForm.Meta.fields + ['short_description']


class StockRecordForm(catalogue_forms.StockRecordForm):
    class Meta(catalogue_forms.StockRecordForm):
        exclude = ('partner_sku', 'price_currency', 'price_retail', 'cost_price')
