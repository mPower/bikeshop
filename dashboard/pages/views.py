# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import
from oscar.apps.dashboard.pages import views
from .forms import PageUpdateForm
from flatpages.models import FlatPage


class PageListView(views.PageListView):
    model = FlatPage


class PageCreateView(views.PageCreateView):
    form_class = PageUpdateForm
    model = FlatPage


class PageUpdateView(views.PageUpdateView):
    form_class = PageUpdateForm
    model = FlatPage
