from django.apps import AppConfig


class SliderDashboardConfig(AppConfig):
    label = 'slider_dashboard'
    name = 'dashboard.slider'
    verbose_name = 'Slider'
