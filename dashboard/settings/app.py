from django.conf.urls import url

from oscar.core.application import DashboardApplication as CoreDashboardApplication
from oscar.core.loading import get_class


class SettingsDashboardApplication(CoreDashboardApplication):
    name = None

    default_permissions = ['is_staff', ]

    settings_list_view = get_class('dashboard.settings.views', 'SettingsView')

    def get_urls(self):
        urls = [
            url(r'^list/$', self.settings_list_view.as_view(), name='settings-list'),
        ]
        return self.post_process_urls(urls)


application = SettingsDashboardApplication()
