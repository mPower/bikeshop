# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from operator import itemgetter
from django.urls import reverse_lazy
from django.views.generic import FormView
from django.utils.formats import localize
from constance.admin import ConstanceForm, get_values
from constance import config, settings


class SettingsView(FormView):
    template_name = 'settings/settings.html'
    form_class = ConstanceForm
    success_url = reverse_lazy('dashboard:settings')

    def __init__(self, *args, **kwargs):
        if 'fields' in kwargs:
            self.fields = kwargs.pop('fields')
        else:
            self.fields = settings.CONFIG.keys()
        super(SettingsView, self).__init__(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(SettingsView, self).get_context_data(**kwargs)
        context['settings'] = []
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        initial = self.get_initial()
        for name, options in settings.CONFIG.items():
            default, help_text = options[0], options[1]
            value = initial.get(name)
            if value is None:
                value = getattr(config, name)

            context['settings'].append({
                'name': name,
                'default': localize(default),
                'help_text': help_text,
                'value': localize(value),
                'modified': value != default,
                'form_field': form[name],
            })
        context['settings'].sort(key=itemgetter('name'))
        return context

    def get_initial(self):
        data = super(SettingsView, self).get_initial()
        data.update(get_values())
        return data

    def form_valid(self, form):
        form.save()
        return self.render_to_response(self.get_context_data(form=form, success_save='ok'))

    def form_invalid(self, form):
        return self.render_to_response(self.get_context_data(form=form, success_save='error'))