# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf.urls import url
from oscar.apps.dashboard.app import DashboardApplication as CoreDashboardApplication
from oscar.core.loading import get_class

# from dashboard.promotions.app import application as app


class DashboardApplication(CoreDashboardApplication):
    slider_app = get_class('dashboard.slider.app', 'application')
    settings_app = get_class('dashboard.settings.app', 'application')
    articles_app = get_class('dashboard.articles.app', 'application')
    # promotions_app = app

    def get_urls(self):
        urls = super(DashboardApplication, self).get_urls()
        urls.append(url(r'^slider/', self.slider_app.urls))
        urls.append(url(r'^settings/', self.settings_app.urls))
        urls.append(url(r'^articles/', self.articles_app.urls))
        return urls


application = DashboardApplication()
