# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import

from oscar.apps.partner import config


class PartnerConfig(config.PartnerConfig):
    name = 'partner'
