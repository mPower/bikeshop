# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import
from oscar.apps.partner import strategy


class Selector(object):
    def strategy(self, request=None, user=None, **kwargs):
        return RusStrategy(request)


class RusStrategy(strategy.UseFirstStockRecord, strategy.StockRequired, strategy.NoTax, strategy.Structured):
    pass
