# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import

from django.apps import AppConfig


class FlatpagesConfig(AppConfig):
    name = 'flatpages'
    label = 'bikeshop_flatpages'
    verbose_name = 'Страницы'
