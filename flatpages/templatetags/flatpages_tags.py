# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import

from django.template import Library
from django.contrib.sites.shortcuts import get_current_site
from ..models import FlatPage

register = Library()


@register.assignment_tag(takes_context=True)
def get_flatpages_menu(context, menu):
    request = context['request']
    site_id = get_current_site(request).id

    return FlatPage.objects.filter(menu=menu, sites=site_id)
