# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import

from django.utils.translation import ugettext_lazy as _
from django.contrib import admin
from django.contrib.flatpages.admin import FlatPageAdmin as DjangoFlatPageAdmin
from django.contrib.flatpages.models import FlatPage as DjangoFlatPage
from .models import FlatPage

admin.site.unregister(DjangoFlatPage)


@admin.register(FlatPage)
class FlatPageAdmin(DjangoFlatPageAdmin):
    fieldsets = (
        (None, {'fields': ('url', 'title', 'menu', 'content', 'sites')}),
        (_('Advanced options'), {
            'classes': ('collapse',),
            'fields': ('registration_required', 'template_name'),
        }),
    )
