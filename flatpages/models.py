# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import

from django.db import models
from django.contrib.flatpages.models import FlatPage as DjangoFlatPage


class FlatPage(DjangoFlatPage):
    MENU_CHOICES = (
        ('', 'Не показывать в меню'),
        ('top', 'Верхнее меню'),
        ('search', 'Рядом с поиском'),
    )
    ICON_CHOICES = (
        ('img/71.jpg', 'Доставка'),
        ('img/72.jpg', 'Тест-драйв'),
        ('img/73.jpg', 'Обслуживание'),
    )
    menu = models.CharField('Меню', max_length=10, choices=MENU_CHOICES)
    icon = models.CharField('Иконка', max_length=50, choices=ICON_CHOICES, blank=True,
                            help_text='Используется только для меню рядом с поиском')

    class Meta:
        verbose_name = 'Страница'
        verbose_name_plural = 'Страницы'
