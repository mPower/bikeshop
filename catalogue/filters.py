# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from django import forms
import django_filters
from oscar.apps.catalogue.models import ProductAttribute
from .models import Product


class ProductFilter(django_filters.FilterSet):
    price_from = django_filters.NumberFilter(name='stockrecords__price_excl_tax', lookup_expr='gte', label='Цена от')
    price_to = django_filters.NumberFilter(name='stockrecords__price_excl_tax', lookup_expr='lte', label='Цена до')

    class Meta:
        model = Product
        fields = ['price_from', 'price_to']

    def __init__(self, *args, **kwargs):
        product_class = None
        if 'product_class' in kwargs:
            product_class = kwargs.pop('product_class')
        super(ProductFilter, self).__init__(*args, **kwargs)
        if product_class:
            for attribute in product_class.attributes.filter(type=ProductAttribute.OPTION):
                self.filters[attribute.code] = django_filters.ModelMultipleChoiceFilter(
                    name='attribute_values__value_option',
                    queryset=attribute.option_group.options.all(),
                    widget=forms.CheckboxSelectMultiple,
                    label=attribute.name
                )
