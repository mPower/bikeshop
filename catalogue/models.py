# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from django.db import models
from django.utils.translation import ugettext_lazy as _
from oscar.apps.catalogue.abstract_models import AbstractCategory, AbstractProduct


class Category(AbstractCategory):
    product_class = models.ForeignKey(
        'catalogue.ProductClass',
        null=True,
        blank=True,
        on_delete=models.PROTECT,
        verbose_name=_('Product type'), related_name="categories",
        help_text=_("Choose what type of product this is"))


class Product(AbstractProduct):
    short_description = models.TextField('Краткое описание', blank=True)


from oscar.apps.catalogue.models import *  # noqa
