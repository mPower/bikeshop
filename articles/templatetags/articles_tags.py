# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import

from django import template
from ..models import Article

register = template.Library()


@register.inclusion_tag('articles/tags/articles_last.html')
def articles_last(count=3):
    articles = Article.objects.filter(is_active=True)[:count]
    return {
        'articles': articles
    }
