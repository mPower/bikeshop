# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.utils.encoding import python_2_unicode_compatible
from django.db import models
from django.utils.translation import ugettext_lazy as _
from oscar.models.fields import AutoSlugField
from redactor.fields import RedactorField
from sorl.thumbnail import ImageField


@python_2_unicode_compatible
class Article(models.Model):
    title = models.CharField('Заголовок', max_length=255)
    image = ImageField('Изображение', upload_to='articles/%Y/%m/%d/')
    text_short = models.TextField(verbose_name='Краткое содержание')
    text = RedactorField(verbose_name='Текст статьи')
    date_created = models.DateTimeField('Дата добавления', auto_now_add=True)
    date_update = models.DateTimeField('Дата обновления', auto_now=True)
    is_active = models.BooleanField('Опубликована', default=True)
    slug = AutoSlugField(_('Slug'), max_length=128, unique=True,
                         populate_from='title')
    views = models.PositiveIntegerField('Количество просмотров', default=0)

    class Meta:
        verbose_name = 'Статья'
        verbose_name_plural = 'Статьи'
        ordering = ['-date_created']

    def __str__(self):
        return self.title

    @models.permalink
    def get_absolute_url(self):
        return ('article-detail', (), {'slug': self.slug})
