# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from decimal import Decimal as D

from oscar.apps.shipping import methods


class SelfPickup(methods.Free):
    code = 'self-pickup'
    name = 'Самовывоз'

    @property
    def description(self):
        from constance import config
        return 'Вы самостоятельно можете забрать Ваш заказ из нашего офиса, расположенного по адресу:<br>{0}'.format(
            config.PICKUP_ADDRESS)


class Courier(methods.Free):
    code = 'courier'
    name = 'Доставка курьером'

    charge_excl_tax = D('100.00')
    charge_incl_tax = D('100.00')

    @property
    def description(self):
        from constance import config
        return config.SHIPPING_COURIER_TEXT
